import { Middleware } from "@koa/router";
import * as application from "../application/index.js";
import { encodeHtml } from "../utils/index.js";

export function createAuthorizeRoute(
    applicationContext: application.Context,
): Middleware {
    return async (context, next) => {
        context.body = `<!DOCTYPE html>
<html>
<head>
<title>Authorize</title>
<script>
document.addEventListener(
    "click",
    event => {
        if (!(event.target instanceof HTMLAnchorElement)) return;

        event.preventDefault();
        location.replace(event.target.href);
    }
);
</script>
</head>

<body>
<h1>Login</h1>
<ul>
    <li><a href="/gitlab/authorize?${encodeHtml(context.querystring)}">Gitlab</a></li>
    <li><a href="/github/authorize?${encodeHtml(context.querystring)}">Github</a></li>
    <li><a href="/google/authorize?${encodeHtml(context.querystring)}">Google</a></li>
</ul>
</body>
</html>        
`;
    };
}
