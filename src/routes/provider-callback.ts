import { Middleware } from "@koa/router";
import assert from "assert";
import * as application from "../application/index.js";
import { OauthProviderBase } from "../providers/index.js";
import { formPostHtml } from "../utils/index.js";

export function createProviderCallbackRoute(
    applicationContext: application.Context,
    provider: OauthProviderBase,
): Middleware {
    return async (context, next) => {
        // TODO validate code_verifier

        const now = new Date();
        const { authClient } = applicationContext.services;

        assert(typeof context.request.query.code === "string");
        assert(typeof context.request.query.state === "string");

        const authorizationRequest = JSON.parse(
            context.request.query.state,
        ) as Oath2CodeAuthorizationRequest;

        const redirectEndpoint = new URL(authorizationRequest.redirect_uri);

        const subject = await provider.getSubject(context.request.query.code);

        /*
        find user
        */
        let user: string;
        try {
            const result = await authClient.resolveUserFromProviderSubject({
                parameters: {
                    provider: provider.type,
                    subject,
                },
            });
            assert(result.status == 200);

            const entity = await result.entity();
            user = entity.user;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        /*
        generate a code
        */
        let code: string;
        let codeExpires: number;
        try {
            const result = await authClient.createAuthorizationCode({
                parameters: {
                    client: authorizationRequest.client_id,
                },
                entity() {
                    return {
                        user,
                    };
                },
            });
            assert(result.status == 201);

            const entity = await result.entity();
            code = entity.code;
            codeExpires = entity.expires;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        const redirectParams = new URLSearchParams();
        redirectParams.append("code", code);
        if (authorizationRequest.state != null) {
            redirectParams.append("state", authorizationRequest.state);
        }

        switch (authorizationRequest.response_mode) {
            case undefined:
            case null:
            case "query": {
                const url = new URL(redirectEndpoint);
                url.search = redirectParams.toString();
                context.redirect(url.toString());
                break;
            }

            case "fragment": {
                const url = new URL(redirectEndpoint);
                url.hash = redirectParams.toString();
                context.redirect(url.toString());
                break;
            }

            case "form_post": {
                context.body = formPostHtml(redirectEndpoint, redirectParams);
                break;
            }

            default:
                throw new application.Oauth2InvalidRequest();

        }
    };
}

interface Oath2CodeAuthorizationRequest {
    client_id: string
    response_type: "code"
    response_mode?: "fragment" | "query" | "form_post"
    redirect_uri: string
    scope: string
    nonce: string
    state?: string
    code_challenge?: string
    code_challenge_method?: "S256" | "none"
}
