import { Middleware } from "@koa/router";
import assert from "assert";
import { second } from "msecs";
import * as application from "../application/index.js";

export function createTokenRoute(
    applicationContext: application.Context,
): Middleware {
    return async (context, next) => {
        const now = new Date();
        const requestEntity = context.request.body as Oauth2TokenRequest;

        if (requestEntity == null) throw new application.Oauth2InvalidRequest();
        if (typeof requestEntity !== "object") throw new application.Oauth2InvalidRequest();

        switch (requestEntity.grant_type) {
            case "authorization_code": {
                const responseEntity = await handleAuthorizationCode(
                    now,
                    requestEntity,
                );

                // TODO make a nicer cors thing
                const redirectUri = new URL(requestEntity.redirect_uri);
                context.response.set("Access-Control-Allow-Origin", redirectUri.origin);
                //

                context.body = responseEntity;
                break;
            }

            case "client_credentials": {
                const authorizationHeader = context.request.header.authorization;

                if (authorizationHeader == null) throw new application.Oauth2UnauthorizedClient();
                if (!authorizationHeader.startsWith("Basic ")) throw new application.Oauth2UnauthorizedClient();

                const [clientId, clientSecret] = Buffer.from(authorizationHeader, "base64").
                    toString("utf8").
                    substring(6).
                    split(":", 2);

                const authorized = await authorizeClient(
                    clientId,
                    clientSecret,
                );

                if (!authorized) throw new application.Oauth2UnauthorizedClient();

                const responseEntity = await handleClientCredentials(
                    now,
                    clientId,
                );
                context.body = responseEntity;
                break;
            }

            default:
                throw new application.Oauth2UnsupportedGrantType();
        }

    };

    async function handleAuthorizationCode(
        now: Date,
        requestEntity: Oauth2AuthorizationCodeTokenRequest,
    ): Promise<Oauth2TokenSuccessfullResponse> {
        const { authClient } = applicationContext.services;

        /*
        first, validate the redirect url
        */
        try {
            const result = await authClient.validateRedirectUri({
                parameters: {
                    client: requestEntity.client_id,
                },
                entity() {
                    return {
                        redirectUri: requestEntity.redirect_uri,
                    };
                },
            });
            assert(result.status == 200);

            const entity = await result.entity();
            assert(entity.valid === true);
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        /*
        exchange code for user id
        */
        let userId: string;
        try {
            const result = await authClient.resolveUserFromAuthorizationCode({
                parameters: {
                    client: requestEntity.client_id,
                },
                entity() {
                    return {
                        code: requestEntity.code,
                    };
                },
            });
            assert(result.status == 200);

            const entity = await result.entity();
            userId = entity.user;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        /*
        create access token
        */
        let accessToken: string;
        let accessTokenExpires: number;
        try {
            const result = await authClient.createAccessToken({
                parameters: {
                    client: requestEntity.client_id,
                },
                entity() {
                    return {
                        user: userId,
                    };
                },
            });
            assert(result.status == 201);

            const entity = await result.entity();
            accessToken = entity.token;
            accessTokenExpires = entity.expires;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        /*
        create refresh token
        */
        let refreshToken: string;
        let refreshTokenExpires: number;
        try {
            const result = await authClient.createRefreshToken({
                parameters: {
                    client: requestEntity.client_id,
                },
                entity() {
                    return {
                        user: userId,
                    };
                },
            });
            assert(result.status == 201);

            const entity = await result.entity();
            refreshToken = entity.token;
            refreshTokenExpires = entity.expires;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        return {
            access_token: accessToken,
            token_type: "bearer",
            expires_in: Math.floor((accessTokenExpires - now.valueOf()) / second),
            refresh_token: refreshToken,
        };
    }

    async function handleClientCredentials(
        now: Date,
        clientId: string,
    ): Promise<Oauth2TokenSuccessfullResponse> {
        const { authClient } = applicationContext.services;

        /*
        create access token
        */
        let accessToken: string;
        let accessTokenExpires: number;
        try {
            const result = await authClient.createAccessToken({
                parameters: {
                    client: clientId,
                },
                entity() {
                    return {
                    };
                },
            });
            assert(result.status == 201);

            const entity = await result.entity();
            accessToken = entity.token;
            accessTokenExpires = entity.expires;
        }
        catch {
            throw new application.Oauth2InvalidGrant();
        }

        return {
            access_token: accessToken,
            token_type: "bearer",
            expires_in: Math.floor((accessTokenExpires - now.valueOf()) / second),
        };
    }

    async function authorizeClient(
        clientId: string,
        clientSecret: string,
    ) {
        const { authClient } = applicationContext.services;

        const validateClientSecretResult = await authClient.validateClientSecret2({
            parameters: {
                client: clientId,
            },
            entity() {
                return { secret: clientSecret };
            },
        });

        if (validateClientSecretResult.status !== 200) return false;

        const validateClientSecretEntity = await validateClientSecretResult.entity();
        return validateClientSecretEntity.valid;
    }

}
type Oauth2TokenRequest =
    Oauth2AuthorizationCodeTokenRequest |
    Oauth2ClientCredentialsAccesTokenRequest;

interface Oauth2AuthorizationCodeTokenRequest {
    "grant_type": "authorization_code";
    "code": string;
    "redirect_uri": string;
    "client_id": string;
    "code_verifier"?: string;
}

interface Oauth2ClientCredentialsAccesTokenRequest {
    "grant_type": "client_credentials";
    "scope"?: string;
}

interface Oauth2TokenSuccessfullResponse {
    "access_token": string;
    "token_type": string;
    "expires_in"?: number;
    "refresh_token"?: string;
    "scope"?: string;
}
