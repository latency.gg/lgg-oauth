import { Middleware } from "@koa/router";
import assert from "assert";
import * as application from "../application/index.js";
import { OauthProviderBase } from "../providers/index.js";

export function createProviderAuthorizeRoute(
    applicationContext: application.Context,
    provider: OauthProviderBase,
): Middleware {
    const { authClient } = applicationContext.services;

    return async (context, next) => {
        const responseType = context.request.query.response_type;

        switch (responseType) {
            case "code": {
                try {
                    const clientId = context.query.client_id;
                    const redirectUri = context.query.redirect_uri;
                    assert(typeof clientId === "string");
                    assert(typeof redirectUri === "string");

                    {
                        const result = await authClient.validateRedirectUri({
                            parameters: {
                                client: clientId,
                            },
                            entity() {
                                return {
                                    redirectUri: redirectUri,
                                };
                            },
                        });
                        assert(result.status == 200);

                        const entity = await result.entity();
                        assert(entity.valid === true);
                    }
                }
                catch {
                    throw new application.Oauth2InvalidGrant();
                }

                const url = await provider.getAuthorizationRedirectEndpoint(
                    JSON.stringify(context.query),
                );
                context.redirect(url.toString());
                break;
            }

            default:
                throw new application.Oauth2UnsupportedResponseType();

        }
    };

}
