import * as authApiSpec from "@latency.gg/lgg-auth-oas";

export function mockResolveUserFromProviderSubject(
    server: authApiSpec.Server,
) {
    server.registerResolveUserFromProviderSubjectOperation(
        async incomingRequest => {
            return {
                status: 200,
                parameters: {},
                entity() {
                    return {
                        user: "AAAB",
                    };
                },
            };
        },
    );
}
