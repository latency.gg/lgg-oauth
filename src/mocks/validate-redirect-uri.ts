import * as authApiSpec from "@latency.gg/lgg-auth-oas";

export function mockValidateRedirectUri(
    server: authApiSpec.Server,
) {
    server.registerValidateRedirectUriOperation(
        async incomingRequest => {
            const entity = await incomingRequest.entity();
            return {
                status: 200,
                parameters: {},
                entity() {
                    return {
                        valid: true,
                    };
                },
            };
        },
    );
}
