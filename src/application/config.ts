import { HttpSendReceive } from "@oas3/http-send-receive";
import * as prom from "prom-client";

export interface Config {
    endpoint: URL;

    authApiEndpoint: URL;

    gitlabClientId: string
    gitlabClientSecret: string

    githubClientId: string
    githubClientSecret: string

    googleClientId: string
    googleClientSecret: string

    abortController: AbortController;
    promRegistry: prom.Registry;
    httpSendReceive?: HttpSendReceive;
}
