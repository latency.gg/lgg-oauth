import * as authApiSpec from "@latency.gg/lgg-auth-oas";
import { Config } from "./config.js";

export interface Services {
    authClient: authApiSpec.Client;
}

export function createServices(
    config: Config,
): Services {
    const authClient = new authApiSpec.Client({
        baseUrl: config.authApiEndpoint,
        httpSendReceive: config.httpSendReceive,
    });

    return {
        authClient,
    };
}
