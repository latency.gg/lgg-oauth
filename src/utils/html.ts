export function formPostHtml(
  action: URL,
  params: URLSearchParams,
) {
  const inputsHtml = Array.from(params.entries()).
    map(([key, value]) => `<input type="hidden" name="${encodeHtml(key)}" value="${encodeHtml(value)}" />`).
    join("\n");

  const formHtml = `
<form method="post" action="${encodeHtml(action.toString())}">
${inputsHtml}
</form>
`;

  const html = `<!DOCTYPE html>
<html>
<head><title>form_post</title></head>
<body onload="document.forms[0].submit()">
${formHtml}
</body>
</html>
`;

  return html;
}

export function encodeHtml(value: string) {
  return value
    .replace("&", "&amp;")
    .replace("\"", "&quot;")
    .replace(">", "&gt;")
    .replace("<", "&lt;");
}

