import { OidcProviderBase } from "./oidc-base.js";

export class GitlabProvider extends OidcProviderBase {
    readonly type = "gitlab";
    readonly issuer = new URL("https://gitlab.com")
}
