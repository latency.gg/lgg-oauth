import assert from "assert";
import fetch from "node-fetch";
import { OauthProviderBase } from "./oauth-base.js";

export interface OidcConfigurationModel {
    authorization_endpoint: string,
    token_endpoint: string,
    userinfo_endpoint: string,
}

export abstract class OidcProviderBase extends OauthProviderBase {
    readonly scope = "openid";
    /**
     * @description Issuers with paths are not supported!!!
     */
    readonly abstract issuer: URL;

    private configurationCache?: Promise<OidcConfigurationModel>;
    private async getConfigurationInternal() {
        const configurationEndpoint = await this.getConfigurationEndpoint();

        const configurationResponse = await fetch(
            configurationEndpoint.toString(),
            {
                method: "GET",
            },
        );
        assert.equal(configurationResponse.status, 200);
        const configurationEntity = await configurationResponse.json() as {
            authorization_endpoint: string,
            token_endpoint: string,
            userinfo_endpoint: string,
        };

        return configurationEntity;
    }

    getConfiguration(): Promise<OidcConfigurationModel> {
        if (this.configurationCache == null) {
            this.configurationCache = this.getConfigurationInternal();
        }
        return this.configurationCache;
    }

    async getConfigurationEndpoint(): Promise<URL> {
        return new URL("/.well-known/openid-configuration", this.issuer);
    }

    async getAuthorizationEndpoint(): Promise<URL> {
        const config = await this.getConfiguration();
        return new URL(config.authorization_endpoint);
    }

    async getTokenEndpoint(): Promise<URL> {
        const config = await this.getConfiguration();
        return new URL(config.token_endpoint);
    }

    async getSubject(
        code: string,
    ) {
        const { clientId, clientSecret, callbackEndpoint } = this;

        const tokenEndpoint = await this.getTokenEndpoint();

        const requestBody = new URLSearchParams();
        requestBody.append("grant_type", "authorization_code");
        requestBody.append("code", code);
        requestBody.append("redirect_uri", callbackEndpoint.toString());
        requestBody.append("client_id", clientId);
        requestBody.append("client_secret", clientSecret);

        const tokenResponse = await fetch(
            tokenEndpoint.toString(),
            {
                method: "POST",
                body: requestBody,
            },
        );
        assert.equal(tokenResponse.status, 200);
        const tokenResponseEntity = await tokenResponse.json() as { id_token: string };

        const textDecoder = new TextDecoder();

        const idToken = tokenResponseEntity.id_token;
        const idTokenParts = idToken.split(".");
        const idTokenPartBuffers = idTokenParts.map(part => Buffer.from(part, "base64"));
        const jwtPayload = JSON.parse(textDecoder.decode(idTokenPartBuffers[1])) as { sub: string };

        return jwtPayload.sub;
    }

}
