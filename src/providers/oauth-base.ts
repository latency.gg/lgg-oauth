import { ProviderBase } from "./base.js";

export abstract class OauthProviderBase extends ProviderBase {
    readonly abstract scope: string

    constructor(
        protected readonly callbackEndpoint: URL,
        protected readonly clientId: string,
        protected readonly clientSecret: string,
    ) {
        super();
    }

    abstract getSubject(
        code: string,
    ): Promise<string>

    abstract getAuthorizationEndpoint(): Promise<URL>

    async getAuthorizationRedirectEndpoint(
        state: string,
    ) {
        const { clientId, callbackEndpoint } = this;

        const authorizationEndpoint = await this.getAuthorizationEndpoint();

        const endpoint = new URL(authorizationEndpoint);
        endpoint.searchParams.append("client_id", clientId);
        endpoint.searchParams.append("redirect_uri", callbackEndpoint.toString());
        endpoint.searchParams.append("response_type", "code");
        endpoint.searchParams.append("scope", this.scope);
        endpoint.searchParams.append("state", state);

        return endpoint;
    }

}
