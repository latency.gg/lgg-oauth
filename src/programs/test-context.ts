import { program } from "commander";
import * as mocks from "../mocks/index.js";
import { withContext } from "../testing/index.js";

program.
    command("test-context").
    action(action);

async function action() {
    console.log("starting...");

    await withContext(async context => {
        mocks.mockValidateRedirectUri(context.servers.auth);
        mocks.mockResolveUserFromProviderSubject(context.servers.auth);
        mocks.mockCreateAuthorizationCode(context.servers.auth);

        console.log("started");

        await new Promise(resolve => process.addListener("SIGINT", resolve));

        console.log("stopping...");
    });

    console.log("stopped");
}
